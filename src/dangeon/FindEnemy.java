package dangeon;

import fighters.ArenaFighters;

public interface FindEnemy {
    void battle(WalkInLabyrinth walkInLabyrinth, ArenaFighters member);
}
