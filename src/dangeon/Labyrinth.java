package dangeon;

import arenas.LabyrinthBattle;
import potions.PotionPoison;
import potions.PotionRecovery;
import fighters.ArenaFighters;

import java.util.Arrays;

public class Labyrinth {
    protected final int MIN_HEIGHT = 10;
    protected final int MIN_WIDTH = 10;
    protected char iconEmptySquare = '\u2610';
    protected char iconLabeledSquare = '\u25A9';
    protected char iconFinish = '\u2FA8';
    protected char iconBarrier = '\u25FC';
    protected char iconSmilingSmiley = '\u263B';
    protected char iconFinishFlag = '\u26F3';
    protected char iconElixirRecovery = '\u271a';
    protected char iconElixirPoison = '\u2620';
    protected char iconEnemy = '\u265e';
    protected int memberCoordinateY = 0;
    protected int memberCoordinateX = 0;
    protected int finishCoordinateY;
    protected int finishCoordinateX;
    protected int counterToWin;
    protected String memberCourse;
    protected char[][] labyrinth;
    protected ArenaFighters[] enemies;
    protected WalkInLabyrinth member;

    protected int amountBarrier = 7;
    protected int amountElixirRecovery = 3;
    protected int amountElixirPoison = 3;
    protected int amountEnemy;
    protected int numEnemy = 0;


    public Labyrinth(int height, int width, WalkInLabyrinth member, ArenaFighters... enemies) {
        if (height < MIN_HEIGHT || width < MIN_WIDTH) {
            creationLabyrinth(MIN_HEIGHT, MIN_WIDTH, member, enemies);
        } else {
            creationLabyrinth(height, width, member, enemies);
        }
    }

    private void creationLabyrinth(int height, int width, WalkInLabyrinth member, ArenaFighters... enemies) {
        labyrinth = new char[height][width];
        finishCoordinateY = height - 1;
        finishCoordinateX = width - 1;
        this.member = member;
        counterToWin = height * width - amountBarrier;
        this.enemies = enemies;
        amountEnemy = enemies.length;
        decorationLabyrinth();
    }

    protected void decorationLabyrinth() {
        for (int i = 0; i < labyrinth.length; i++) {
            for (int j = 0; j < labyrinth[i].length; j++) {
                labyrinth[i][j] = iconEmptySquare;
            }
        }
        creatingObstacles();
        creatingFinish();
    }

    protected void creatingObstacles() {
        creatingNumberOfDecorElements(amountBarrier, iconBarrier);
        creatingNumberOfDecorElements(amountElixirRecovery, iconElixirRecovery);
        creatingNumberOfDecorElements(amountElixirPoison, iconElixirPoison);
        creatingNumberOfDecorElements(amountEnemy, iconEnemy);
    }

    protected void creatingNumberOfDecorElements(int num, char icon) {
        for (int i = 0; i < num; i++) {
            creatingOneElementOfDecor(icon);
        }
    }

    protected void creatingFinish() {
        labyrinth[finishCoordinateY][finishCoordinateX] = iconFinish;

    }

    public void startWalk() {
        while (!isGameOver()) {
            markWay();
            seeMap();
            movementMember();
        }
    }

    protected boolean isGameOver() {
        if ((memberCoordinateY == finishCoordinateY) && (memberCoordinateX == finishCoordinateX)) {
            markWay();
            seeMap();
            System.out.println(iconFinishFlag + " The member found the way out of the labyrinth! " +
                    iconFinishFlag + "\n" + "Not visited places " + counterToWin);
            return true;
        } else if ((counterToWin == 0)) {
            System.out.println("The labyrinth passed completely!");
            return true;
        } else if (member.getHP() < 0) {
            System.out.println("Your hero is dead " + '\u2639');
            return true;
        }
        return false;
    }

    protected void markWay() {
        if (isReplaceIcon()) {
            labyrinth[memberCoordinateY][memberCoordinateX] = iconLabeledSquare;
            counterToWin();
        } else if (labyrinth[memberCoordinateY][memberCoordinateX] == iconFinish) {
            counterToWin();
            labyrinth[memberCoordinateY][memberCoordinateX] = iconSmilingSmiley;
        }
    }

    protected boolean isReplaceIcon() {
        return isIconInCell(memberCoordinateY, memberCoordinateX, iconEmptySquare)
                || isIconInCell(memberCoordinateY, memberCoordinateX, iconElixirRecovery)
                || isIconInCell(memberCoordinateY, memberCoordinateX, iconElixirPoison)
                || isIconInCell(memberCoordinateY, memberCoordinateX, iconEnemy);
    }

    protected void seeMap() {
        for (char[] lab : labyrinth) {
            System.out.println(Arrays.toString(lab));
        }
    }

    protected void movementMember() {
        int[][] coordinates = generationRandomMove();
        String access;
        if (isSuccessfulMovement(coordinates)) {
            readCoordinatesMember(coordinates);
            eventsOnWay();
            access = " Path is free.";
        } else {
            returnMemberBack();
            access = " On a way Deadlock remain on [" + memberCoordinateY + "]" +
                    "[" + memberCoordinateX + "].";
        }
        announcement(memberCourse, access, coordinates);
    }

    protected void counterToWin() {
        counterToWin--;
    }

    protected int[][] generationRandomMove() {
        int[][] coordinatesMovement = null;
        int randomMove = (int) ((Math.random() * 4));
        switch (randomMove) {
            case 0:
                memberCourse = "Forward";
                coordinatesMovement = member.goForward();
                break;
            case 1:
                memberCourse = "Backward";
                coordinatesMovement = member.goBackward();
                break;
            case 2:
                memberCourse = "Right";
                coordinatesMovement = member.goRight();
                break;
            case 3:
                memberCourse = "Left";
                coordinatesMovement = member.goLeft();
                break;
        }
        return coordinatesMovement;

    }

    protected boolean isSuccessfulMovement(int[][] coordinatesMovement) {
        if (coordinatesMovement == null || coordinatesMovement[0].length < 2) {
            return false;
        } else if ((coordinatesMovement[0][0] < labyrinth.length && coordinatesMovement[0][0] >= 0)
                && (coordinatesMovement[0][1] < labyrinth[0].length && coordinatesMovement[0][1] >= 0)) {
            return !(isIconInCell(coordinatesMovement[0][0], coordinatesMovement[0][1], iconBarrier));
        } else {
            return false;
        }
    }

    protected boolean isIconInCell(int coordinateY, int coordinateX, char icon) {
        return labyrinth[coordinateY][coordinateX] == icon;
    }

    protected void readCoordinatesMember(int[][] coordinatesMovement) {
        memberCoordinateY = coordinatesMovement[0][0];
        memberCoordinateX = coordinatesMovement[0][1];
    }

    protected void returnMemberBack() {
        member.setCoordinateY(memberCoordinateY);
        member.setCoordinateX(memberCoordinateX);
    }

    protected void eventsOnWay() {
        if (isIconInCell(memberCoordinateY, memberCoordinateX, iconElixirRecovery)) {
            openElixir(new PotionRecovery());
        } else if (isIconInCell(memberCoordinateY, memberCoordinateX, iconElixirPoison)) {
            openElixir(new PotionPoison());
        } else if (isIconInCell(memberCoordinateY, memberCoordinateX, iconEnemy)) {
            startBattle(new LabyrinthBattle());
        }
    }

    protected void creatingOneElementOfDecor(char icon) {
        int y = (int) (Math.random() * (finishCoordinateY - 1) + 1);
        int x = (int) (Math.random() * (finishCoordinateX - 1) + 1);
        if (isIconInCell(y, x, iconEmptySquare)) {
            labyrinth[y][x] = icon;
        } else {
            creatingOneElementOfDecor(icon);
        }
    }

    protected void announcement(String direction, String access, int[][] coordinate) {
        int coordinateY = memberCoordinateY;
        int coordinateX = memberCoordinateX;
        if (coordinate != null && coordinate[0].length > 1) {
            coordinateY = coordinate[0][0];
            coordinateX = coordinate[0][1];
        }
        System.out.println("The member tries to move " + direction + " to the place [" + coordinateY + "]" +
                "[" + coordinateX + "]. " + access + "\n" + "Member's remaining life is " + (int) member.getHP() +
                " hp." + "\n" + "Not visited places " + counterToWin + ".");
    }

    protected void startBattle(FindEnemy enemy) {
        enemy.battle(member, enemies[numEnemy++]);
    }

    protected void openElixir(FindPotion findPotion) {
        findPotion.open(member);
    }
}
