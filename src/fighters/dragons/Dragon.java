package fighters.dragons;

import dangeon.WalkInLabyrinth;
import fighters.ArenaFighters;
import fighters.Elements;
import fighters.dragonRiders.DragonRider;

public class Dragon extends ArenaFighters implements Elements, WalkInLabyrinth {
    private int params;
    private int coordinateX = 0;
    private int coordinateY = 0;

    public Dragon(String name, float health, float damage, float armor, int params) {
        super(name, health, damage, armor);
        this.params = params;
    }

    private float attackDragon(Dragon dragon) {
        float damageTaken = 0f;
        int degreeCritDamage = 3;
        float superDamage = calculationChanceCritDamage(this.damage, degreeCritDamage);
        if ((params & FIRE) == FIRE && (dragon.params & WATER) == WATER) {
            damageTaken += superDamage * 2;
        } else damageTaken += superDamage;
        if ((params & WATER) == WATER && (dragon.params & EARTH) == EARTH) {
            damageTaken += superDamage * 2;
        } else damageTaken += superDamage;
        if ((params & EARTH) == EARTH && (dragon.params & WIND) == WIND) {
            damageTaken += superDamage * 2;
        } else damageTaken += superDamage;
        if ((params & WIND) == WIND && (dragon.params & FIRE) == FIRE) {
            damageTaken += superDamage * 2;
        } else damageTaken += superDamage;

        return dragon.damaged(damageTaken);
    }

    private float calculationChanceCritDamage(float damage, int degreeCritDamage) {
        int randomNum = (int) (Math.random() * 10) + 1;
        if (randomNum == 10) {
            return (float) Math.pow(damage, degreeCritDamage);
        }
        return damage;
    }

    @Override
    public float attack(ArenaFighters fighters) {
        if (fighters instanceof Dragon) {
            return attackDragon((Dragon) fighters);
        } else if (!(fighters instanceof DragonRider)) {
            return fighters.damaged(this.damage);
        } else
            return 0;
    }

    @Override
    public int getElements() {
        return params;
    }

    @Override
    public int[][] goForward() {
        coordinateX++;
        return new int[][]{{coordinateY, coordinateX}};
    }

    @Override
    public int[][] goBackward() {
        coordinateX--;
        return new int[][]{{coordinateY, coordinateX}};
    }

    @Override
    public int[][] goRight() {
        coordinateY++;
        return new int[][]{{coordinateY, coordinateX}};
    }

    @Override
    public int[][] goLeft() {
        coordinateY--;
        return new int[][]{{coordinateY, coordinateX}};
    }

    @Override
    public void setCoordinateY(int coordinateY) {
        this.coordinateY = coordinateY;
    }

    @Override
    public void setCoordinateX(int coordinateX) {
        this.coordinateX = coordinateX;
    }

    @Override
    public void setHP(float health) {
        this.setHealth(health);
    }

    @Override
    public float getHP() {
        return this.getHealth();
    }

    @Override
    public float getMaxHP() {
        return this.getMaxHealth();
    }
}
