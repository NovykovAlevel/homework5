package fighters.knights;

import fighters.PostAttackAction;

public class BlackKnight extends Knight implements PostAttackAction{
    public BlackKnight(String name, float health, float damage, float armor, float shield) {
        super(name, health, damage, armor, shield);
    }

    private void recovery(float damage) {
        if (this.isAlfie()) {
            float recovery = damage / 2;
            this.heal(recovery);
            System.out.println(this.getName() + " recovery " + recovery);
        }
    }


    @Override
    public void postAttackAction(float damageGiven, float damageGotten) {
        recovery(damageGiven);
    }


}
