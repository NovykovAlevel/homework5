package arenas;

import fighters.ArenaFighters;
import fighters.PostAttackAction;
import healers.Healer;


public abstract class BattleArena {
    protected static final int MIN_COUNT_ROUND = 3;
    private boolean startRound = false;
    protected Healer healer;
    protected GodHand godHand;

    public BattleArena() {
    }

    public BattleArena(Healer healer) {
        this.healer = healer;
    }

    public abstract void startBattle();

    public void setGodHand(GodHand godHand) {
        this.godHand = godHand;
    }

    public abstract void printWinner();

    public ArenaFighters calculationOfWinner(ArenaFighters member1, ArenaFighters member2) {
        if (member1 != null && member2 != null) {
            if (member1.isAlfie() && member2.isAlfie()) {
                return (member1.getHealth() > member2.getHealth()) ? member1 : member2;
            } else if (member1.isAlfie()) {
                return member1;
            } else if (member2.isAlfie()) {
                return member2;
            }
        }
        return null;
    }

    public boolean isStartRound() {
        return startRound;
    }

    protected void setStartRound(boolean startRound) {
        this.startRound = startRound;
    }

    protected void fight(ArenaFighters member1, ArenaFighters member2) {
        if (isFightContinue(member1, member2)) {
            float dam1 = member1.attack(member2);
            float dam2 = member2.attack(member1);
            if (member1 instanceof PostAttackAction) {
                ((PostAttackAction) member1).postAttackAction(dam1, dam2);
            }

            if (member2 instanceof PostAttackAction) {
                ((PostAttackAction) member2).postAttackAction(dam2, dam1);
            }

            if (healer != null) {
                healer.heal(dropTheCoin() ? member1 : member2);
            }

            if (godHand != null) {
                godHand.godHand(member1, member2);
            }
        }
    }

    protected boolean dropTheCoin() {
        int randomNum = (int) (Math.random() * 100);
        return (randomNum % 2) == 0;
    }

    protected boolean isFightContinue(ArenaFighters member1, ArenaFighters member2) {
        if (member1 != null && member2 != null) {
            return member1.isAlfie() && member2.isAlfie();
        } else {
            return false;
        }
    }

    public interface GodHand {
        boolean godHand(ArenaFighters member1, ArenaFighters member2);
    }
}
