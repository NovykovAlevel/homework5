package arenas;

import fighters.*;
import healers.Healer;


public class Tournament extends BattleArena {
    private int maxRoundCount;
    private ArenaFighters[] tournamentWinner;
    private ArenaFighters[] members;

    public Tournament(Healer healer, ArenaFighters member1, ArenaFighters member2, ArenaFighters member3,
                      ArenaFighters member4, ArenaFighters member5, ArenaFighters member6,
                      ArenaFighters member7, ArenaFighters member8, int maxRoundCount) {
        super(healer);
        this.maxRoundCount = maxRoundCount;
        members = new ArenaFighters[]{member1, member2, member3, member4, member5,
                member6, member7, member8};
    }

    public int getNumberOfMembers() {
        return members.length;
    }

    @Override
    public void startBattle() {
        tournamentWinner = battleInTournament(members);
    }

    @Override
    public void printWinner() {
        if (tournamentWinner != null) {
            System.out.println("The winner of tournament becomes " + tournamentWinner[0].getName());
        }
    }

    private ArenaFighters[] battleInTournament(ArenaFighters[] members){
        int index = 0;
        int counter = 0;
        if (members.length > 1) {
            ArenaFighters[] winners = new ArenaFighters[members.length / 2];
            for (int i = 1; i < members.length; i++) {
                if (i % 2 == 0) {
                    continue;
                }

                while (counter < maxRoundCount) {
                    fight(members[i-1], members[i]);
                    counter++;
                }

                ArenaFighters win = calculationOfWinner(members[i-1], members[i]);
                winners[index++] = win;
                System.out.println("<<<" + members[i-1].getName() + " VS " + members[i].getName() + ">>>" +
                        " WINS " + win.getName());
            }
            members = battleInTournament(winners);
        }
        return members;
    }
}
