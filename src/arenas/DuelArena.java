package arenas;

import fighters.ArenaFighters;
import healers.Healer;

public class DuelArena extends BattleArena {
    protected ArenaFighters member1;
    protected ArenaFighters member2;
    private int maxRoundCount;


    public DuelArena(Healer healer, ArenaFighters member1, ArenaFighters member2, int maxRoundCount) {
        super(healer);
        this.member1 = member1;
        this.member2 = member2;
        this.maxRoundCount = maxRoundCount;
    }

    @Override
    public void startBattle() {
        setStartRound(true);
        int currentRound = 0;
        while (currentRound < maxRoundCount && isFightContinue(member1, member2)) {
            fight(member1, member2);
            currentRound++;
        }
    }

    @Override
    public void printWinner() {
        if (isStartRound()) {
            ArenaFighters winners = calculationOfWinner(member1, member2);
            if (winners != null) {
                System.out.println("The winner of the battle is " + winners.getName() + "!");
            } else {
                System.out.println("Not all members gathered at the start!");
            }

        } else {
            System.out.println("The battle has not yet begun!");
        }
    }
}
